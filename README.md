# Instructions to download

```
setupATLAS
lsetup git

git clone --recursive https://:@gitlab.cern.ch:8443/sosen/buckets_genlevel_efficiency.git

```




# Instructions to compile and run

compile:
```
mkdir build
cd build

setupATLAS
asetup AnalysisTop,21.2.69

cmake ..
make

souce setup.sh
cd ..
```

run:
```
mkdir run
cd run
main
```
