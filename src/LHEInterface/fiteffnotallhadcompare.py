import ROOT
from math import sqrt
from array import array
from fiteff import *

def main(lines,chtype):
   d = dictmaker(lines[1:])
   #print d
   eff, err = effdicts(d)
   #print eff
   #print err
   g = ROOT.TGraphErrors(len(err), array('f', eff.keys()), array('f', eff.values()), array('f', [0]*len(err)), array('f', err.values()))
   g.GetXaxis().SetTitle("Wwt")
   g.GetYaxis().SetTitle("buckets algo. efficiency")
   g.SetMarkerSize(1)
   g.SetMarkerStyle(8)
   #g.SetMarkerColor(2)
   g.SetTitle(chtype)
   return g

if __name__=="__main__":
   #chtype = "notallhad"
   #smearwidth=[0.8, 1.0, 1.2]
   #targetmtW=[80,85,90]
   targetmtW=86
   #smearwidth = [1.0,1.2]
   sw = 1.0
   #sw = 1.2
   ROOT.gROOT.SetBatch(1)
   c = ROOT.TCanvas( 'c1', 'c1', 200, 10, 600, 400 )
   leg = ROOT.TLegend(0.65, 0.8, 0.90, 0.9)
   counter = 0
   chtype1 = "notallhad"
   #chtype1 = "notallhad_correctB2masstarget"
   #ROOT.gPad.SetLogx(1)
   mg = ROOT.TMultiGraph()
   labels = {"old":"/afs/cern.ch/work/s/sosen/ChongbinTop/SAB_new/build/", "afterMerge": ""}
   for label in labels:
      chtype = "notallhadsw%s"%str(sw)
      #chtype = "notallhadsw%s_correctB2masstarget"%str(sw)
      f = open("%seffWwt%s_correctB2masstarget_fakeleptonunsmeared_newmetric_targetMTW%i_lepconstraint.txt"%(labels[label], chtype, targetmtW), 'r')
      #f = open("effWwt%s_correctB2masstarget_fakeleptonunsmeared_newmetric_targetMTW%i.txt"%(chtype, tmtw), 'r')
      eb1wt = f.readlines()
      g = main(eb1wt, chtype)
      g.SetMarkerSize(0.5)
      g.SetMarkerColor(counter + 2)
      mg.Add(g, "p")
      leg.AddEntry(g, label, "p")
      #leg.AddEntry(g, str(tmtw)+" GeV", "p")
      counter += 1
   mg.Draw("a")
   mg.SetTitle("smearwith=%f"%sw)
   mg.GetXaxis().SetTitle("Wwt")
   mg.GetYaxis().SetTitle("buckets algo. efficiency")
   leg.Draw()
   c.Print("effvsWwt%s_sw%f_compare_aftercodechange.eps"%(chtype1, sw))
   #c.Print("effvsWwt%s_targetMTWcomparison.eps"%(chtype1))
   #mg.Draw("a")
   #c.Print("effvsB1wt%s_logx.eps"%(chtype1))

